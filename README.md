# Bootstrap4 ContainerRow Paragraph

This module closely follows Bootstrap4 grid system.

After installation two paragraph-types are created:

1. Bootstrap4 ContainerRow
2. Bootstrap4 Column

# Bootstrap4 ContainerRow

* This paragraph type has *full width* and *background* options.
* The *full width* option uses Bootstrap4' fluid-container.
* The *background* option has 4 options (transparent + 3 colors) and you can easily change these values after installing this module.
The *background* option uses little css/scss for styling. You can change this (@see Theming).

# Bootstrap4 Column

* This paragraph type has *width* and *background* options.
* The *width* option uses Bootstrap4' grid columns like col-sm-12. This way it's trivial to set up a grid for your content pages.
* The *background* option has 4 options (transparent + 3 colors) and you can easily change these values after installing this module.
The *background* option uses little css/scss for styling. You can change this (@see Theming).

When creating new paragraph-types you can re-use the *width* and *background* fields. This module handles rendering them as css-classes without additional custom coding.

# Theming

Overide libraries in your custom theme
https://www.drupal.org/node/2497313

* bootstrap4_container_row_paragraph/column-background-colors
* bootstrap4_container_row_paragraph/containerrow-background-colors

# Comparision with other modules

* https://www.drupal.org/project/bootstrap_paragraphs has more preconfigured paragraphs. It does not have a Row paragraph-type.
