<?php

/**
 * @file
 * Contains bootstrap4_container_row_paragraph.module.
 */

use Drupal\Component\Utility\Xss;
use Drupal\Core\Routing\RouteMatchInterface;
use Erusev\Parsedown\Parsedown;

/**
 * Implements hook_help().
 */
function bootstrap4_container_row_paragraph_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the bootstrap4_container_row_paragraph module.
    case 'help.page.bootstrap4_container_row_paragraph':
      $parsedown = new Parsedown();
      $readme = file_get_contents(drupal_get_path('module', 'bootstrap4_container_row_paragraph') . '/README.md');
      return Xss::filterAdmin($parsedown->text($readme));

    default:
  }
}

/**
 * Implements hook_theme().
 */
function bootstrap4_container_row_paragraph_theme($existing, $type, $theme, $path) {
  return [
    'paragraph__bs4crp_containerrow' => [
      'base hook' => 'paragraph',
    ],
    'field__paragraph__bs4crp_containerrow_paragraphs' => [
      'base hook' => 'paragraph',
    ],
  ];
}

/**
 * Implements TEMPLATE_preprocess_HOOK().
 */
function bootstrap4_container_row_paragraph_preprocess_paragraph(&$variables) {
  $paragraph = $variables['paragraph'];


  /**
   * The containerrow paragraph has a field 'background-class'.
   *
   * We add the field-value as class on the paragraph.
   * Further rendering happens in the custom template: "paragraph--bs4crp-containerrow.html.twig"
   *
   * The custom css-library "bootstrap4_container_row_paragraph/containerrow-background-colors" is attached in the
   * custom template above.
   */
  if (
    $paragraph->hasField('bs4crp_containerrow_bg_class') &&
    $style = $paragraph->get('bs4crp_containerrow_bg_class')->getString()
  ) {
    $variables['attributes']['class'][] = $style;
  }

  /**
   * The column paragraph has a field 'background-class'.
   *
   * We add the field-value as class on the paragraph.
   * Further rendering happens in the default paragraphs template.
   *
   * Because we don't control the default paragraphs template, the custom css-library
   * "bootstrap4_container_row_paragraph/column-background-colors" is attached inside
   * the preprocess function.
   *
   * Because this field can be re-used in multiple paragraph-type the field is removed
   * from the twig render array here. This is done for convenience.
   */
  if (
    $paragraph->hasField('bs4crp_column_bg_class') &&
    $style = $paragraph->get('bs4crp_column_bg_class')->getString()
  ) {
    $variables['attributes']['class'][] = $style;

    // Add the custom css-library here.
    $variables['#attached']['library'][] = 'bootstrap4_container_row_paragraph/column-background-colors';

    // Remove from twig {{content}} render array.
    if (isset($variables['content']['bs4crp_column_bg_class'])) {
      unset($variables['content']['bs4crp_column_bg_class']);
    }
  }

  /**
   * The column paragraph has a field 'width-class'.
   *
   * We add the field-value as class on the paragraph.
   * Further rendering happens in the default paragraphs template.
   *
   * The field-classes are part of the Bootstrap4 grid-system.
   * So we don't need an additional css-library to handle these css-classes.
   *
   * Because this field can be re-used in multiple paragraph-type the field is removed
   * from the twig render array here. This is done for convenience.
   */
  if (
    $paragraph->hasField('bs4crp_column_width') &&
    $style = $paragraph->get('bs4crp_column_width')->getString()
  ) {
    $variables['attributes']['class'][] = $style;

    // Remove from twig {{content}} render array.
    if (isset($variables['content']['bs4crp_column_width'])) {
      unset($variables['content']['bs4crp_column_width']);
    }
  }

}
